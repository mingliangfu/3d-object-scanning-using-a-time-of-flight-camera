#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/surface/mls.h>
#include <pcl/surface/impl/mls.hpp>

#include <pcl/io/vtk_lib_io.h>

typedef pcl::PointXYZRGB PointT;

int
main (int argc, char** argv)
{
  // Load input file into a PointCloud<T> with an appropriate type
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_0 (new pcl::PointCloud<pcl::PointXYZRGB>);
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_1 (new pcl::PointCloud<pcl::PointXYZRGB>);
  //pcl::PCLPointCloud2 cloud_blob;
  pcl::io::loadPCDFile ("3D_object.pcd", *cloud_0);
  //pcl::fromPCLPointCloud2 (cloud_blob, *cloud);
  //* the data should be available in cloud

  pcl::VoxelGrid<PointT> grid;
  grid.setLeafSize (5.0f, 5.0f,5.0f);
  grid.setInputCloud (cloud_0);
  grid.filter (*cloud_1);

  	// Create the filtering object
	pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
	sor.setInputCloud (cloud_1);
	sor.setMeanK (/*50*/50);
	sor.setStddevMulThresh (/*1.0*/1.0);
	//sor.setNegative (true);
	sor.filter (*cloud);

  // Normal estimation*
  pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> n;
  pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud (cloud);
  n.setInputCloud (cloud);
  n.setSearchMethod (tree);
  n.setKSearch (20);
  n.compute (*normals);
  //* normals should not contain the point normals + surface curvatures

  // Concatenate the XYZ and normal fields*
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
  pcl::concatenateFields (*cloud, *normals, *cloud_with_normals);
  //* cloud_with_normals = cloud + normals

  // Create search tree*
  pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointXYZRGBNormal>);
  tree2->setInputCloud (cloud_with_normals);

  //// Smoothing and normal estimation based on polynomial reconstruction
  //pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr mls_points (new pcl::PointCloud<pcl::PointXYZRGBNormal>);

  //  // Init object (second point type is for the normals, even if unused)
  //pcl::MovingLeastSquares<pcl::PointXYZRGB, pcl::PointXYZRGBNormal> mls;
 
  //mls.setComputeNormals (true);

  //// Set parameters
  //mls.setInputCloud (cloud);
  //mls.setPolynomialFit (true);
  //mls.setSearchMethod (tree);
  //mls.setSearchRadius (/*0.03*/4);


  //  mls.setPolynomialOrder (1);
  //  //mls.setUpsamplingMethod (MovingLeastSquares<PointXYZ, PointXYZ>::SAMPLE_LOCAL_PLANE);
  //  mls.setUpsamplingRadius (1);
  //  mls.setUpsamplingStepSize (0.3);

  //// Reconstruct
  //mls.process (*mls_points);

  //pcl::visualization::PCLVisualizer visu2;
  //pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> mls_rgb(mls_points);
  //visu2.addPointCloud(mls_points, mls_rgb);
  //visu2.spin();
  //// Smoothing and normal estimation based on polynomial reconstruction - End

  // Initialize objects
  pcl::GreedyProjectionTriangulation<pcl::PointXYZRGBNormal> gp3;
  pcl::PolygonMesh triangles;

  // Set the maximum distance between connected points (maximum edge length)
  gp3.setSearchRadius (/*0.025*/25);

  // Set typical values for the parameters
  gp3.setMu (2.5);
  gp3.setMaximumNearestNeighbors (100);
  gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
  gp3.setMinimumAngle(M_PI/18); // 10 degrees
  gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
  gp3.setNormalConsistency(/*false*/true);

  // Get result
  gp3.setInputCloud (cloud_with_normals);
  gp3.setSearchMethod (tree2);
  gp3.reconstruct (triangles);

  // Additional vertex information
  std::vector<int> parts = gp3.getPartIDs();
  std::vector<int> states = gp3.getPointStates();

  //pcl::io::saveVTKFile ("mesh.vtk", triangles);
  //pcl::io::savePolygonFileSTL("mesh.stl", triangles);
  //pcl::io::savePolygonFilePLY("mesh.ply", triangles);
  pcl::visualization::PCLVisualizer visu;
  visu.setBackgroundColor (255, 255, 255);
  visu.addPolygonMesh(triangles);
  visu.spin();
  // Finish
  return (0);
}