/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 *
 */

/* \author Radu Bogdan Rusu
 * adaptation Raphael Favier*/

#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>

#include <pcl/io/pcd_io.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>

#include <pcl/features/normal_3d.h>

#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/registration/ia_ransac.h>
#include "pcl/features/fpfh.h"
#include <pcl/features/fpfh_omp.h>
//#include <pcl/registration/sample_consensus_prerejective.h>

using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;

//convenient typedefs
typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;
typedef pcl::PointNormal PointNormalT;
typedef pcl::PointCloud<PointNormalT> PointCloudWithNormals;
typedef pcl::FPFHSignature33 DescriptorT;
typedef pcl::Normal NormalT;
typedef pcl::KdTreeFLANN<pcl::PointXYZRGB> SearchMethod;

// This is a tutorial so we can afford having global variables 
	//our visualizer
	pcl::visualization::PCLVisualizer *p;
	//its left and right viewports
	int vp_1, vp_2;

//convenient structure to handle our pointclouds
struct PCD
{
  PointCloud::Ptr cloud;
  std::string f_name;

  PCD() : cloud (new PointCloud) {};
};

struct PCDComparator
{
  bool operator () (const PCD& p1, const PCD& p2)
  {
    return (p1.f_name < p2.f_name);
  }
};


// Define a new point representation for < x, y, z, curvature >
class MyPointRepresentation : public pcl::PointRepresentation <PointNormalT>
{
  using pcl::PointRepresentation<PointNormalT>::nr_dimensions_;
public:
  MyPointRepresentation ()
  {
    // Define the number of dimensions
    nr_dimensions_ = 4;
  }

  // Override the copyToFloatArray method to define our feature vector
  virtual void copyToFloatArray (const PointNormalT &p, float * out) const
  {
    // < x, y, z, curvature >
    out[0] = p.x;
    out[1] = p.y;
    out[2] = p.z;
    out[3] = p.curvature;
  }
};


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the first viewport of the visualizer
 *
 */
void showCloudsLeft(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source)
{
  p->removePointCloud ("vp1_target");
  p->removePointCloud ("vp1_source");
  p->setBackgroundColor (255, 255, 255);
  //PointCloudColorHandlerCustom<PointT> tgt_h (cloud_target, 0, 255, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> tgt_h(cloud_target);
  //PointCloudColorHandlerCustom<PointT> src_h (cloud_source, 255, 0, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> src_h(cloud_source);
  p->addPointCloud (cloud_target, tgt_h, "vp1_target", vp_1);
  p->addPointCloud (cloud_source, src_h, "vp1_source", vp_1);

  PCL_INFO ("Press q to begin the registration.\n");
  p-> spin();
}


////////////////////////////////////////////////////////////////////////////////
/** \brief Display source and target on the second viewport of the visualizer
 *
 */
void showCloudsRight(const PointCloud::Ptr cloud_target, const PointCloud::Ptr cloud_source)
{
  p->removePointCloud ("source");
  p->removePointCloud ("target");
  p->setBackgroundColor (255, 255, 255);

  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> tgt_color_handler (cloud_target);
  if (!tgt_color_handler.isCapable ())
      PCL_WARN ("Cannot create curvature color handler!");

  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> src_color_handler (cloud_source);
  if (!src_color_handler.isCapable ())
      PCL_WARN ("Cannot create curvature color handler!");


  p->addPointCloud (cloud_target, tgt_color_handler, "target", vp_2);
  p->addPointCloud (cloud_source, src_color_handler, "source", vp_2);

  p->spinOnce();
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Load a set of PCD files that we want to register together
  * \param argc the number of arguments (pass from main ())
  * \param argv the actual command line arguments (pass from main ())
  * \param models the resultant vector of point cloud datasets
  */
void loadData (int argc, char **argv, std::vector<PCD, Eigen::aligned_allocator<PCD> > &models)
{
  std::string extension (".pcd");
  // Suppose the first argument is the actual test model
  for (int i = 1; i < argc; i++)
  {
    std::string fname = std::string (argv[i]);
    // Needs to be at least 5: .plot
    if (fname.size () <= extension.size ())
      continue;

    std::transform (fname.begin (), fname.end (), fname.begin (), (int(*)(int))tolower);

    //check that the argument is a pcd file
    if (fname.compare (fname.size () - extension.size (), extension.size (), extension) == 0)
    {
      // Load the cloud and saves it into the global list of models
      PCD m;
      m.f_name = argv[i];
      pcl::io::loadPCDFile (argv[i], *m.cloud);
      //remove NAN points from the cloud
      std::vector<int> indices;
      pcl::removeNaNFromPointCloud(*m.cloud,*m.cloud, indices);

      models.push_back (m);
    }
  }
}

void printMatix4f(const Eigen::Matrix4f & matrix) {

	printf ("Rotation matrix :\n");
	printf ("    | %6.3f %6.3f %6.3f | \n", matrix (0,0), matrix (0,1), matrix (0,2));
	printf ("R = | %6.3f %6.3f %6.3f | \n", matrix (1,0), matrix (1,1), matrix (1,2));
	printf ("    | %6.3f %6.3f %6.3f | \n", matrix (2,0), matrix (2,1), matrix (2,2));
	printf ("Translation vector :\n");
	printf ("t = < %6.3f, %6.3f, %6.3f >\n\n", matrix (0,3), matrix (1,3), matrix (2,3));
}

////////////////////////////////////////////////////////////////////////////////
/** \brief Align a pair of PointCloud datasets and return the result
  * \param cloud_src the source PointCloud
  * \param cloud_tgt the target PointCloud
  * \param output the resultant aligned source PointCloud
  * \param final_transform the resultant transform between source and target
  */
void pairAlign (const PointCloud::Ptr cloud_src, const PointCloud::Ptr cloud_tgt, PointCloud::Ptr output, Eigen::Matrix4f &final_transform, bool downsample = false)
{
  //
  // Downsample for consistency and speed
  // \note enable this for large datasets
  PointCloud::Ptr src (new PointCloud);
  PointCloud::Ptr tgt (new PointCloud);
  pcl::VoxelGrid<PointT> grid;
  if (downsample)
  {
    grid.setLeafSize (6.0f, 6.0f, 6.0f);
    grid.setInputCloud (cloud_src);
    grid.filter (*src);

    grid.setInputCloud (cloud_tgt);
    grid.filter (*tgt);
  }
  else
  {
    src = cloud_src;
    tgt = cloud_tgt;
  }

  // Align
  pcl::IterativeClosestPoint<PointT, PointT> reg;
  reg.setTransformationEpsilon (/*1e-6*/1e-6);
  // Set the maximum distance between two correspondences (src<->tgt) to 10cm
  // Note: adjust this based on the size of your datasets
  reg.setMaxCorrespondenceDistance (/*0.1*/55.0);  
  reg.setRANSACOutlierRejectionThreshold(50);
  // Set the point representation
  //reg.setPointRepresentation (boost::make_shared<const MyPointRepresentation> (point_representation));

  reg.setInputCloud (src);
  reg.setInputTarget (tgt);

  //
  // Run the same optimization in a loop and visualize the results
  Eigen::Matrix4f Ti = Eigen::Matrix4f::Identity (), prev, targetToSource;
  PointCloud::Ptr reg_result = src;
  reg.setMaximumIterations (2);

  //pcl::PointCloud<int> sampled_indices;
  //pcl::PointCloud<PointT>::Ptr src_keypoints (new pcl::PointCloud<PointT> ());
  //pcl::PointCloud<PointT>::Ptr tgt_keypoints (new pcl::PointCloud<PointT> ());
  //pcl::PointCloud<NormalT>::Ptr src_normals (new pcl::PointCloud<NormalT> ());
  //pcl::PointCloud<NormalT>::Ptr tgt_normals (new pcl::PointCloud<NormalT> ());
  //pcl::PointCloud<DescriptorT>::Ptr src_descriptors (new pcl::PointCloud<DescriptorT> ());
  //pcl::PointCloud<DescriptorT>::Ptr tgt_descriptors (new pcl::PointCloud<DescriptorT> ());

  //  // Estimate normals
  //pcl::console::print_highlight ("Estimating scene normals...\n");
  //pcl::NormalEstimationOMP<PointT, pcl::Normal> nest;
  //nest.setRadiusSearch (10);
  //nest.setInputCloud (tgt);
  //nest.compute (*tgt_normals);

  //nest.setInputCloud (src);
  //nest.compute (*src_normals);

    // Estimate features
  //pcl::console::print_highlight ("Estimating features...\n");
  //pcl::FPFHEstimationOMP<PointT, pcl::Normal, DescriptorT> fest;
  //fest.setRadiusSearch (25.0);
  //fest.setInputCloud (src);
  //fest.setInputNormals (src_normals);
  //fest.compute (*src_descriptors);
  //fest.setInputCloud (tgt);
  //fest.setInputNormals (tgt_normals);
  //fest.compute (*tgt_descriptors);

 //   // Perform alignment
 // pcl::console::print_highlight ("Starting alignment...\n");
 // pcl::SampleConsensusInitialAlignment<PointT, PointT, DescriptorT> align;
 // align.setInputCloud (src);
 // align.setSourceFeatures (src_descriptors);
 // align.setInputTarget (tgt);
 // align.setTargetFeatures (tgt_descriptors);
 // pcl::PointCloud<PointT>::Ptr aligned_source (new pcl::PointCloud<PointT> ());
 // align.setNumberOfSamples (3); // Number of points to sample for generating/prerejecting a pose
 // align.setCorrespondenceRandomness (2); // Number of nearest features to use
 // //align.setSimilarityThreshold (0.6f); // Polygonal edge length similarity threshold
 // align.setMaxCorrespondenceDistance (25.0); // Set inlier threshold
 // //align.setInlierFraction (0.25f); // Set required inlier fraction
 // align.align (*aligned_source);
 // 	Eigen::Matrix4f initial_T = align.getFinalTransformation ();	//printMatix4f(initial_T);	//Ti = initial_T;	//pcl::transformPointCloud (*src, *reg_result, initial_T);
	////reg_result = aligned_source;
	//showCloudsRight(tgt, reg_result);

  // The SampleConsensusInitialAlignment API
	//pcl::SampleConsensusInitialAlignment<PointT, PointT, DescriptorT> sac;
	//sac.setMinSampleDistance (50);
	//sac.setMaxCorrespondenceDistance (55.0);
	//sac.setMaximumIterations (50);
	////Provide a pointer to the input point cloud and features
	//sac.setInputCloud (src);
	//sac.setSourceFeatures (src_descriptors);
	////Provide a pointer to the target point cloud and features
	//sac.setInputTarget (tgt);
	//sac.setTargetFeatures (tgt_descriptors);
	////Align input to target to obtain
	//pcl::PointCloud<PointT>::Ptr aligned_source (new pcl::PointCloud<PointT> ());
	//sac.align (*aligned_source);
	//Eigen::Matrix4f initial_T = sac.getFinalTransformation ();	//printMatix4f(initial_T);	//Ti = initial_T;	//pcl::transformPointCloud (*src, *reg_result, initial_T);
	//reg_result = aligned_source;
	//showCloudsRight(tgt, reg_result);
	//p->spin();

  for (int i = 0; i < 30; ++i)
  {
    PCL_INFO ("Iteration Nr. %d.\n", i);

    // save cloud for visualization purpose
    src = reg_result;

    // Estimate
    reg.setInputCloud (src);
    reg.align (*reg_result);

		//accumulate transformation between each Iteration
    Ti = reg.getFinalTransformation () * Ti;

		//if the difference between this transformation and the previous one
		//is smaller than the threshold, refine the process by reducing
		//the maximal correspondence distance
    if (fabs ((reg.getLastIncrementalTransformation () - prev).sum ()) < reg.getTransformationEpsilon ())
      reg.setMaxCorrespondenceDistance (reg.getMaxCorrespondenceDistance () - /*0.001*/0.001);
    
    prev = reg.getLastIncrementalTransformation ();

    // visualize current state
    showCloudsRight(tgt, src);
  }

	//
  // Get the transformation from target to source
  targetToSource = Ti.inverse();

  //
  // Transform target back in source frame
  pcl::transformPointCloud (*cloud_tgt, *output, targetToSource);
  	showCloudsRight(cloud_tgt, output);

  p->removePointCloud ("source");
  p->removePointCloud ("target");

  //PointCloudColorHandlerCustom<PointT> cloud_tgt_h (output, 0, 255, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> cloud_tgt_h(output);
  //PointCloudColorHandlerCustom<PointT> cloud_src_h (cloud_src, 255, 0, 0);
  pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> cloud_src_h(cloud_src);
  p->addPointCloud (output, cloud_tgt_h, "target", vp_2);
  p->addPointCloud (cloud_src, cloud_src_h, "source", vp_2);

  PCL_INFO ("Press q to continue the registration.\n");
  p->spin ();

  p->removePointCloud ("source"); 
  p->removePointCloud ("target");

  //add the source to the transformed target
  //*output += *cloud_src;
  
  final_transform = targetToSource;
 }


/* ---[ */
int main (int argc, char** argv)
{
  // Load data
  std::vector<PCD, Eigen::aligned_allocator<PCD> > data;
  loadData (argc, argv, data);

  // Check user input
  if (data.empty ())
  {
    PCL_ERROR ("Syntax is: %s <source.pcd> <target.pcd> [*]", argv[0]);
    PCL_ERROR ("[*] - multiple files can be added. The registration results of (i, i+1) will be registered against (i+2), etc");
    return (-1);
  }
  PCL_INFO ("Loaded %d datasets.", (int)data.size ());
  
  // Create a PCLVisualizer object
  p = new pcl::visualization::PCLVisualizer (argc, argv, "Pairwise Incremental Registration example");
  p->createViewPort (0.0, 0, 0.5, 1.0, vp_1);
  p->createViewPort (0.5, 0, 1.0, 1.0, vp_2);

	PointCloud::Ptr result (new PointCloud), source, target;
	PointCloud::Ptr final_result (new PointCloud);
	PointCloud::Ptr temp_0, temp_1, temp_2, temp_3, result_0, result_1, result_2 (new PointCloud);

  Eigen::Matrix4f GlobalTransform = Eigen::Matrix4f::Identity (), pairTransform, firstTransform;

  for (size_t j = 0; j < data[0].cloud->points.size (); ++j)
	{
		final_result->points.push_back(data[0].cloud->points[j]);
	}
  
  for (size_t i = 1; i < data.size (); ++i)
  {
    source = data[i-1].cloud;
    target = data[i].cloud;

	//result->clear();

    // Add visualization data
    showCloudsLeft(source, target);

    PointCloud::Ptr temp (new PointCloud);
    PCL_INFO ("Aligning %s (%d) with %s (%d).\n", data[i-1].f_name.c_str (), source->points.size (), data[i].f_name.c_str (), target->points.size ());
    pairAlign (source, target, temp, pairTransform, true);

    //transform current pair into the global transform
    pcl::transformPointCloud (*temp, *result, GlobalTransform);

    //update the global transform
    GlobalTransform = GlobalTransform * pairTransform;

	//save aligned pair, transformed into the first cloud's frame
    //std::stringstream ss;
    //ss << i << ".pcd";
    //pcl::io::savePCDFile (ss.str (), *result, true);

 //   PCL_INFO ("Aligning %s (%d) with %s (%d).\n", data[i-1].f_name.c_str (), source->points.size (), data[i].f_name.c_str (), target->points.size ());
	//pairAlign (source, result_0, result_1, pairTransform, true);

	////transform current pair into the global transform
 //   pcl::transformPointCloud (*result_1, *result, GlobalTransform);

 //   //update the global transform
 //   GlobalTransform = GlobalTransform * pairTransform;

	for (size_t j = 0; j < result->points.size (); ++j)
	{
		final_result->points.push_back(result->points[j]);
	}

	//pcl::io::savePCDFile ("3D_object.pcd", *final_result, true);

	//pcl::visualization::PCLVisualizer visu;
	//pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> cloud_final_rgb(final_result);
	//visu.setBackgroundColor (255, 255, 255);
	//visu.addPointCloud<pcl::PointXYZRGB> (final_result, cloud_final_rgb);
	//visu.spin();

	//for (size_t j = 0; j < result->points.size (); ++j)
	//{
	//	final_result->points.push_back(result->points[j]);
	//}
  }

 //   for (size_t i = 1; i < data.size ()-1; ++i)
	//{
	//	temp_0->clear();
	//	temp_0 = data[i-1].cloud; 
	//	temp_1->clear();
	//	temp_1 = data[i].cloud; 

	//	PCL_INFO ("Aligning %s (%d) with %s (%d).\n", i, temp_0->points.size (), i+1, temp_1->points.size ());
	//	pairAlign (temp_0, temp_1, temp_2, pairTransform, true);

	//	pcl::transformPointCloud (*temp_2, *temp_3, firstTransform);

	//	firstTransform = firstTransform * pairTransform;

	//	for (size_t j = 0; j < temp_3->points.size (); ++j)
	//	{
	//		final_result->points.push_back(temp_3->points[j]);
	//	}

	//	pcl::visualization::PCLVisualizer visu;
	//	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> cloud_final_rgb(final_result);
	//	visu.addPointCloud<pcl::PointXYZRGB> (final_result, cloud_final_rgb);
	//	visu.spin();
	//}

    pcl::io::savePCDFile ("3D_object.pcd", *final_result, true);
	pcl::visualization::PCLVisualizer visu;
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> cloud_final_rgb(final_result);
	visu.addPointCloud<pcl::PointXYZRGB> (final_result, cloud_final_rgb);
	visu.spin();

}
/* ]--- */